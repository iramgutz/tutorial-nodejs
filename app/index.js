import server from '@/server';
import db from '@/DB';
import routes from '@/Routes';

db.initialize();
routes.initialize();


server.listen(3000 , () => console.log('Server listen on port 3000') );