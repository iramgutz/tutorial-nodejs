import mongoose from 'mongoose';
import loadClass from 'mongoose-class-wrapper';
import uniqueValidator from 'mongoose-unique-validator';

let MemberSchema = new mongoose.Schema({
    name : {
        type: String,
        required : true,
    },
    email: {
        type: String,
        required : true,
        unique: true
    },
    created_at: {
        type: Date
    },
    updated_at: {
        type: Date
    }
});


class MemberEntity {

    getFillable()
    {
        return ['name' , 'email' , 'created_at', 'updated_at'];
    }

}

MemberSchema.plugin(uniqueValidator)
MemberSchema.plugin(loadClass, MemberEntity)

export default mongoose.model('Member', MemberSchema)