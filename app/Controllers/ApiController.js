import Q from 'q';

class ApiController {
    constructor(){

    }

    index(req , res){

        return this.
            _firstPromise()
            .then(response => res.json(response) , 200)
            .catch(err => res.json(err), 500)
    }

    hello(req , res){
        res.json({
            message: 'Hello '+req.params.name
        });
    }

    store(req , res){
        res.json(req.body);
    }

    _firstPromise(){

        let deferred = Q.defer();

        setTimeout(() => deferred.resolve({status: "Ok"}), 2000);


        return deferred.promise;
    }
}

export default ApiController;