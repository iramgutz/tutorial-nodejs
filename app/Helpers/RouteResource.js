import BaseController from '@/Controllers/BaseController';
import bodyParser from 'body-parser';

var jsonParser = bodyParser.json()
var urlencodedParser = bodyParser.urlencoded({ extended: true });

class RouteResource
{
    create(router , controller)
    {
        if(controller instanceof BaseController === true)
        {
            router.get('/', (req , res) => controller.index(req , res) );
            router.post('/', urlencodedParser, jsonParser , (req , res) => controller.store(req , res) );
            router.get('/:id', (req , res) => controller.show(req , res) );
            router.put('/:id', urlencodedParser, jsonParser , (req , res) => controller.update(req , res) );
            router.delete('/:id', (req , res) => controller.delete(req , res) );
        }
        else
        {
            console.log('Controller tiene que ser una instacia de BaseController.');
        }

        return router;
    }
}

export default RouteResource;
