import Base from '@/Validators/BaseValidator'
import validate from 'mongoose-validator';

class MemberValidator extends Base
{
  constructor(Entity){
    super(Entity);

    this.entity.schema.path('name').required(true);
    this.entity.schema.path('email').required(true);
    this.entity.schema.path('created_at').required(true);
    this.entity.schema.path('updated_at').required(true);

    this.entity.schema.path('name').validate(validate({
      validator: 'isLength',
      arguments: [3, 50]
    }));

  }
}

export default MemberValidator
