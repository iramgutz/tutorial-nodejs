import Q from 'q'

export default class BaseValidator
{
  constructor(Entity){
    this.entity = Entity;
  }

  isValid()
  {
        var deferred = Q.defer();
        this.entity.validate( error => {

          if(error)
          {
            this.errors = this._generateErrors(error);
            deferred.reject(this._generateErrors(error));
          }
          else{
              deferred.resolve();
          }

        });

        return deferred.promise;
  }

  setEntity(Entity)
  {
      this.entity = Entity;
  }

  _generateErrors(err)
  {
      err = err.errors;

      var errors = {};

      Object.keys(err).forEach(function (e) {
        let error = err[e];
        errors[e] = error.message;
      });

      return errors;
  }

}
