import app from '@/app';
import members from '@/Routes/MemberRoutes';

class Routes {
    constructor(app)
    {
        this.app = app;
    }
    initialize()
    {
        app.use(function(req, res, next) {
          res.header("Access-Control-Allow-Origin", "*");
          res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
          res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
          next();
        });
        app.use('/members', members);
    }
}

export default new Routes(app);
