# Tutorial NodeJS

## Nuevo proyecto

Comenzamos generando un nuevo proyecto

```
npm init
```

Podemos instalar cualquier modulo disponible en https://www.npmjs.com/, algunos de los modulos que mas utilizaremos son:

* nodemon
* babel
* babel-cli
* express
* mongoose

```
npm install nodemon babel babel-cli express mongoose --save
```

Utilizamos express como router de nuestra aplicacion

```
app.get('/', function(req, res){
    console.log(res.send('Hello world!'));
});
```

Podemos generar diferentes routers para  diferentes propositos

```
var api = express.Router();

api.get('/', function(req, res){
   res.json({status: 'ok'});
});

api.get('/:name', (req, res) =>  res.json({ message: 'Hello '+req.params.name}) );

app.use('/api', api);
```

Utilizamos el modulo de body-parser para obtener información adicional del request

```
npm install body-parser --save
```

Este paquete se uso a modo de middleware en las rutas definidad

```
import bodyParser from 'body-parser'

var urlencodedParser = bodyParser.urlencoded({ extended: true });

api.post('/', urlencodedParser , (req , res) => {
    res.json(req.body);
});
```

Otro paquete importante para facilitar la utilización de modulos propios es babel-root-import


```
npm install --save babel-root-import
```

Se agrega un archivo de configuración llamado .babelrc

```
{
  "plugins": [
    ["babel-root-import", {
      "rootPathPrefix": ".",
      "rootPathSuffix": "app"
    }],
    "babel-plugin-transform-decorators-legacy"
  ],
  "presets": [
    "es2015"
  ]
}
```

Una de las caracteristicas mas interesantes de EcmaScript 6 son la integración de Clases

```
class ApiController {
    constructor(){

    }

    index(req , res){
        res.json({
            status: 'Ok'
        });
    }

    hello(req , res){
        res.json({
            message: 'Hello '+req.params.name
        });
    }

    store(req , res){
        res.json(req.body);
    }
}

export default ApiController;
```

Uno de los paquetes mas importantes es q para manejar peticiones asincronas como promesas, debido a la naturaleza asincrona de JavaScript

```
npm install --save q
```


```
import Q from 'q';
```

```
index(req , res){

    return this.
        _firstPromise()
        .then(response => res.json(response) , 200)
        .catch(err => res.json(err), 500)
}
```

```
_firstPromise(){

        let deferred = Q.defer();

        setTimeout(() => deferred.resolve({status: "Ok"}), 2000);


        return deferred.promise;
    }
```

La mayoria de proyectos de NodeJS cuentan con una capa de persistencia en MongoDB, para esto, utilizamos el paquete mongoose

```
npm install --save mongoose
```

```
mongoose.connect('mongodb://localhost/tutorial_nodejs');

import MemberController from './Controllers/MemberController';

var memberController = new MemberController;
```

```
var members = express.Router();

members.get('/', (req, res) => memberController.index(req, res) );
members.post('/', urlencodedParser , (req , res) => memberController.store(req , res) );
members.get('/:id', (req , res) => memberController.show(req , res) );
members.put('/:id', urlencodedParser , (req , res) => memberController.update(req , res) );
members.delete('/:id', (req , res) => memberController.delete(req , res) );

app.use('/members' , members);
```

Schema

```
import mongoose from 'mongoose';

let MemberSchema = new mongoose.Schema({
    name : {
        type: String,
        required : true
    },
    email : {
        type: String,
        required : true
    }
},
{
    toObject: {
        virtuals: true
    },
    toJSON: {
        virtuals: true
    }
});

export default mongoose.model('Member', MemberSchema);
```

Controller

```
import MemberSchema from './Schemas/MemberSchema';

class MemberController {
    constructor(){
        this.schema = MemberSchema;
    }

    index(req , res){

        return this
            .schema
            .find({})
            .then(resources => res.status(200).json(resources))
            .catch(err => res.status(500).json({ errors : err}));
    }

    store(req , res){

        let resource = new this.schema;

        resource.name = req.body.name || '';
        resource.email = req.body.email || '';

        return resource
                .save(req.body)
                .then(resource => res.status(201).json(resource))
                .catch(err => res.status(500).json({ errors : err}));
    }

    show(req,res){

        return this
            .schema
            .findOne({_id: req.params.id})
            .then(resource => {

                if(!resource)
                {
                    res.status(404).json({error: "Entity not found"});
                }
                else
                {
                    res.status(200).json(resource)
                }
            })
            .catch(err => res.status(500).json({ errors : err}));
    }

    update(req,res){

        return this
                .schema
                .findOne({_id: req.params.id})
                .then(resource => {
                    if(!resource)
                    {
                        res.status(404).json({error: "Entity not found"});
                    }
                    else
                    {
                        resource.name = req.body.name || resource.name;
                        resource.email = req.body.email || resource.email;
                        return resource.save();
                    }
                })
                .then(resource => res.status(200).json(resource) )
                .catch(err => res.status(500).json({ errors : err}));
    }


    delete(req,res){

        return this
                .schema
                .findOne({_id: req.params.id})
                .then(resource => {
                    if(!resource)
                    {
                        res.status(404).json({error: "Entity not found"});
                    }
                    else
                    {
                        return resource.remove();
                    }
                })
                .then(resource => res.sendStatus(204))
                .catch(err => res.status(500).json({ errors : err}));
    }
}

export default MemberController;
```

## Arquitectura de capas

![Arquitectura de capas](https://raw.githubusercontent.com/iramgutierrez/tutorial-laravel/master/layers.png)

Agregamos una carpeta por cada una de las capas mostradas, ademas de la carpeta de Routes y Helpers.






